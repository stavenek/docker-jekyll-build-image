# README #

This is a project to create a stable build image for local or bitbucket pipelines usage.
Contains:
* zip
* unzip
* awscli
* python
* jekyll
* bundler
* gem
* curl


## Build docker image
 
```
#!bash

  docker build -t purpur/jekyll .

```

## Use docker image
  
```
#!bash

  docker run -v $(pwd):/srv/jekyll -it purpur/jekyll /bin/bash

```


# run as part of build locally
- bundler install
- jekyll build
- RUN aws s3 sync ./_site s3://www.mycoolwebsite.com/ --acl public-read  --delete