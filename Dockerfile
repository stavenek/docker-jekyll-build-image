# Build:
#   docker build -t purpur/jekyll .
#
# Use:
#    docker run -v $(pwd):/srv/jekyll -it purpur/jekyll /bin/bash
#
# Use this image both locally and/or in your pipeline
#
# === run as part of build locally
# bundler install
# jekyll build
# aws s3 sync ./_site s3://www.website.com/ --acl public-read  --delete


FROM jekyll/jekyll:pages

RUN apk update && apk add --upgrade curl && apk add --update python &&  apk add --update unzip && apk add python-dev

RUN gem install bundler
# RUN bundler install

#          - jekyll build
#          - ls -la


RUN curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "/tmp/awscli-bundle.zip"

RUN ls -la /tmp

RUN unzip /tmp/awscli-bundle.zip -d /tmp/awscli/
RUN /tmp/awscli/awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws

WORKDIR /srv/jekyll
